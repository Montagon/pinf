-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 18-12-2014 a las 18:51:08
-- Versión del servidor: 5.5.40-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `pcomp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `COMPANIAS`
--

CREATE TABLE IF NOT EXISTS `COMPANIAS` (
  `id_comp` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_comp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MODERADORES`
--

CREATE TABLE IF NOT EXISTS `MODERADORES` (
  `id_mod` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `es_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_mod`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `OFERTAS`
--

CREATE TABLE IF NOT EXISTS `OFERTAS` (
  `id_oferta` int(11) NOT NULL AUTO_INCREMENT,
  `id_comp` int(11) NOT NULL,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `id_tarifa` int(11) NOT NULL,
  PRIMARY KEY (`id_oferta`),
  KEY `id_comp` (`id_comp`),
  KEY `id_tarifa` (`id_tarifa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PERIODOS`
--

CREATE TABLE IF NOT EXISTS `PERIODOS` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `id_comp` int(11) NOT NULL,
  `id_oferta` int(11) NOT NULL,
  `termino` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_periodo`),
  KEY `id_comp` (`id_comp`),
  KEY `id_oferta` (`id_oferta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TARIFAS_ACCESO`
--

CREATE TABLE IF NOT EXISTS `TARIFAS_ACCESO` (
  `id_tarifa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_tarifa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `OFERTAS`
--
ALTER TABLE `OFERTAS`
  ADD CONSTRAINT `OFERTAS_ibfk_1` FOREIGN KEY (`id_comp`) REFERENCES `COMPANIAS` (`id_comp`) ON DELETE CASCADE,
  ADD CONSTRAINT `OFERTAS_ibfk_2` FOREIGN KEY (`id_tarifa`) REFERENCES `TARIFAS_ACCESO` (`id_tarifa`) ON DELETE CASCADE;

--
-- Filtros para la tabla `PERIODOS`
--
ALTER TABLE `PERIODOS`
  ADD CONSTRAINT `PERIODOS_ibfk_1` FOREIGN KEY (`id_comp`) REFERENCES `COMPANIAS` (`id_comp`) ON DELETE CASCADE,
  ADD CONSTRAINT `PERIODOS_ibfk_2` FOREIGN KEY (`id_oferta`) REFERENCES `OFERTAS` (`id_oferta`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
